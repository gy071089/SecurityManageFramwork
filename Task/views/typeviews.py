# -*- coding: utf-8 -*-
# @Time    : 2020/11/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : typeviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import xssfilter
from .. import models, serializers


@api_view(['GET'])
def select_type_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    list_get = models.Type.objects.all()
    list_count = list_get.count()
    serializers_get = serializers.TypeSelectSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def select_predefine_views(request, type_id):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    if type_id == '0':
        list_get = models.PreDefine.objects.all()
    else:
        type_get = models.Type.objects.filter(id=type_id)
        list_get = models.PreDefine.objects.filter(tasktype=type_get)
    list_count = list_get.count()
    serializers_get = serializers.PreDefineSerializer(instance=list_get, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)