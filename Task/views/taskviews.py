# -*- coding: utf-8 -*-
# @Time    : 2020/9/30
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : taskviews.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import serializers, models, forms
from ..Functions import taskfun
from Base.models import Person
from Asset.models import Asset
from Workflow.Functions import workflowfun


@api_view(['GET'])
def list_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    list_get = taskfun.get_task_list(request.user).filter(name__icontains=key).order_by('-update_time')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.TaskListSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def delete_views(request, task_id):
    data = {
        "code": 1,
        "msg": "",
    }
    task_item = taskfun.check_task_permission(task_id, request.user)
    if task_item:
        task_item.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '权限错误'
    return JsonResponse(data)


@api_view(['GET'])
def details_views(request, task_id):
    data = {
        "code": 1,
        "msg": "",
    }
    task_item = taskfun.check_task_permission(task_id, request.user)
    if task_item:
        task_item.delete()
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '权限错误'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    form = forms.TaskEditForm(request.POST)
    if form.is_valid():
        task_get = models.Task.objects.create(
            name=form.cleaned_data['name'],
            target=form.cleaned_data['target'],
            type=form.cleaned_data['type'],
            person=form.cleaned_data['person'],
            user=request.user,
            predefine=form.cleaned_data['predefine'],
            start_time=form.cleaned_data['start_time'],
            end_time = form.cleaned_data['end_time'],
        )
        if form.cleaned_data['cycle_hours']:
            task_get.cycle_hours = form.cleaned_data['cycle_hours']
        if form.cleaned_data['manage']:
            manage_list = Person.objects.filter(
                id__in=form.cleaned_data['manage'].split(',')
            )
            task_get.manage.add(*manage_list)
        if form.cleaned_data['asset']:
            asset_list = Asset.objects.filter(
                id__in=form.cleaned_data['asset'].split(',')
            )
            task_get.asset.add(*asset_list)
        task_get.save()
        res = workflowfun.create_workflow(request.user, 'task', task_get)
        if res:
            data['code'] = 0
            data['msg'] = 'success'
        else:
            task_get.delete()
            data['msg'] = '流程启动失败'
    else:
        data['msg'] = '请检查输入'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def update_views(request, task_id):
    data = {
        "code": 1,
        "msg": "",
    }
    task_item = taskfun.check_task_permission(task_id, request.user)
    if task_item:
        form = forms.TaskEditForm(request.POST)
        if form.is_valid():
            task_item.name = form.cleaned_data['name']
            task_item.target = form.cleaned_data['target']
            task_item.type = form.cleaned_data['type']
            task_item.person = form.cleaned_data['person']
            task_item.user = request.user,
            task_item.start_time = form.cleaned_data['start_time']
            if form.cleaned_data['manage']:
                task_item.manage.clear()
                manage_list = Person.objects.filter(
                    id__in=form.cleaned_data['manage'].split(',')
                )
                task_item.manage.add(*manage_list)
            if form.cleaned_data['asset']:
                task_item.asset.clear()
                asset_list = Asset.objects.filter(
                    id__in=form.cleaned_data['asset'].split(',')
                )
                task_item.manage.add(*asset_list)
            task_item.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '权限错误'
    return JsonResponse(data)
