# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : Forms.py

from django.forms import ModelForm
from . import models


class BuildingProjectsUpdateForm(ModelForm):
    class Meta:
        model = models.Building_Projects
        fields = ('name', 'person', 'description', 'level', 'startdate', 'enddate')


class BuildingProjectsActionForm(ModelForm):
    class Meta:
        model = models.Building_Projects_Action
        fields = ('action', 'person', 'actiontime')


class BuildingPhishingUpdateForm(ModelForm):
    class Meta:
        model = models.Building_Phishing
        fields = ('name', 'number', 'scope', 'description')


class BuildingEventUpdateForm(ModelForm):
    class Meta:
        model = models.Building_Event
        fields = ('name', 'person', 'description','scope', 'lose', 'level', 'startdate', 'enddate')


class BuildingEventActionForm(ModelForm):
    class Meta:
        model = models.Building_Event_Action
        fields = ('action', 'person', 'actiontime')


