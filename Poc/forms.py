# -*- coding: utf-8 -*-
# @Time    : 2020/6/16
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : Forms.py

from django.forms import ModelForm
from django import forms
from . import models


class PocUpdateForm(ModelForm):
    class Meta:
        model = models.POC
        fields = ('name', 'level', 'introduce', 'description', 'fix', 'poc')


class PocTestForm(forms.Form):
    host = forms.CharField()
