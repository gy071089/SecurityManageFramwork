# coding:utf-8
from rest_framework.decorators import api_view
from django.http import JsonResponse
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.views.decorators.csrf import csrf_protect
from .. import models,forms
from .. import serializers
from datetime import datetime
from ..Functions import pocfun
from Base.Functions import basefun


# Create your views here.
@api_view(['GET'])
def mainlist(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    starttime = request.GET.get('starttime', '1999-1-1')
    endtime = request.GET.get('endtime', datetime.now())
    list_get = models.POC.objects.filter(name__icontains=key,
                                         is_check=True,
                                         update_data__gte=starttime,
                                         update_data__lte=endtime).order_by('-update_data')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.PocSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def checklist(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    starttime = request.GET.get('starttime', '1999-1-1')
    endtime = request.GET.get('endtime', datetime.now())
    list_get = models.POC.objects.filter(name__icontains=key,
                                         is_check=False,
                                         update_data__gte=starttime,
                                         update_data__lte=endtime).order_by('-update_data')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.PocSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)