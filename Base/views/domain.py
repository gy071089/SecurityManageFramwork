# -*- coding: utf-8 -*-
# @Time    : 2020/6/24
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : domain.py

from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models, forms
from .. import serializers
from django.views.decorators.csrf import csrf_protect


@api_view(['GET'])
def domain_list(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        key = request.GET.get('key', '')
        list_get = models.Domain.objects.filter(key__icontains=key).order_by('update_time')
        list_count = list_get.count()
        # pg = MyPageNumberPagination()
        # list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.DomainSerializer(instance=list_get, many=True)
        data['code'] = 0
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['GET'])
def domain_delete(request, domain_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        item_get = models.Domain.objects.filter(id=domain_id).first()
        if item_get:
            item_get.delete()
            data['code'] = 0
            data['msg'] = '操作成功'
        else:
            data['msg'] = '你要干啥'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def domain_create(request):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user.is_superuser:
        form = forms.DomainForm(request.POST)
        if form.is_valid():
            form.save()
            # tasks.get_dns_asset(form.cleaned_data['key'])
            # tasks.get_oneforall_asset(form.cleaned_data['key'])
            data['code'] = 0
            data['msg'] = '添加成功'
        else:
            data['msg'] = '请检查参数'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def domain_update(request, domain_id):
    data = {
        "code": 1,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    if user:
        domain_get = models.Domain.objects.filter(id=domain_id).first()
        if domain_get:
            form = forms.DomainForm(request.POST, instance=domain_get)
            if form.is_valid():
                form.save()
                data['code'] = 0
                data['msg'] = '添加成功'
            else:
                data['msg'] = '请检查参数'
        else:
            data['msg'] = '指定参数不存在'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)
