# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py
from django import forms


class CheckReasonForms(forms.Form):
    description = forms.CharField()


class CheckManyForms(forms.Form):
    list = forms.CharField()
    description = forms.CharField()